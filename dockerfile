FROM python:3.9

RUN apt-get update 

WORKDIR /contentores


COPY . .

RUN pip install requests
RUN pip install schedule

CMD [ "python", "./app.py" ]
